var qr_text='';

function bug(msg){
    console.log(msg);
}


//var contexts = ["page","selection","link","editable","image","video","audio"];
chrome.contextMenus.create({"title": '将选中文本转换为二维码', "contexts":['selection'],
                                     "id": "context_selection" });
                                     
chrome.contextMenus.create({"title": '将链接地址转换为二维码', "contexts":['link'],
                                     "id": "context_link" });                  
 


chrome.contextMenus.onClicked.addListener(onClickHandler);
 
function onClickHandler(info, tab) {
 
    var text=null;  
    if(info.menuItemId=='context_selection' ){
    
        text=info.selectionText;
        
    }
    if( info.menuItemId=='context_link'){
        console.log(info);
        text=info.linkUrl;
    }
    if(text){
        //sessionStorage.setItem("qr_text", text);
        qr_text=text;
        bug(sessionStorage);
        chrome.tabs.create({url:"popup.html"});
        
    }
};
//call from popup
function getQRtext(){
    return qr_text;
}