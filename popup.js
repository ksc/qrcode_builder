var $textarea=$("#textinfo"); 

var _text=chrome.extension.getBackgroundPage().getQRtext()
if(_text){
    $textarea.val(_text).change();  

}

$textarea.change(function(){
    build_qrcode();
    
});  
  

jQuery(function(){
	build_qrcode();
})
$('#btn').click(function(){
    
    build_qrcode();
});
$('#btn_seturl').click(function(){
    setCurrentUrl();
});
function build_qrcode(){
    var str=$textarea.val();
    str=utf16to8(str);
    console.log(str);
    $('#output').html('');
    jQuery('#output').qrcode(str);

}  
function setCurrentUrl(){
   chrome.windows.getCurrent(function(w){
        chrome.tabs.query({'active':true,'windowId':w.id},function (tab){
            //console.log('tab',tab);
            $textarea.val(tab[0].url).change();    
        });
   })

}
function utf16to8(str) {
    var out, i, len, c;
    out = "";
    len = str.length;
    for(i = 0; i < len; i++) {
	c = str.charCodeAt(i);
	if ((c >= 0x0001) && (c <= 0x007F)) {
	    out += str.charAt(i);
	} else if (c > 0x07FF) {
	    out += String.fromCharCode(0xE0 | ((c >> 12) & 0x0F));
	    out += String.fromCharCode(0x80 | ((c >>  6) & 0x3F));
	    out += String.fromCharCode(0x80 | ((c >>  0) & 0x3F));
	} else {
	    out += String.fromCharCode(0xC0 | ((c >>  6) & 0x1F));
	    out += String.fromCharCode(0x80 | ((c >>  0) & 0x3F));
	}
    }
    return out;
}
